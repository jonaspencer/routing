import { Link } from "react-router-dom";

const Products = () => {
  return (
    <section>
      <h1>
        <b>We have wares!</b>
      </h1>
      <ul>
        <li>
          <Link to="/products/Tome"> Ancient Tome</Link>
        </li>
        <li>
          <Link to="/products/Dew"> Mountain Dew</Link>
        </li>
        <li>
          <Link to="/products/Debt"> Debt</Link>
        </li>
      </ul>
    </section>
  );
};

export default Products;
