import { Route } from "react-router-dom";

const Welcome = () => {
  return (
    <section>
      <h1>
        <b>Welcome to the Page!</b>
      </h1>
      <Route path="/welcome/new">
        <p>Hi!!</p>
      </Route>
    </section>
  );
};

export default Welcome;
