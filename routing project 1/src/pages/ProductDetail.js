import { useParams } from "react-router";

const ProductDetail = () => {
  const params = useParams();
  return (
    <h1>
      <b>Product Deets!</b>
      <p>{params.id}</p>
    </h1>
  );
};

export default ProductDetail;
