const NotFound = () => {
  return (
    <div className="centered">
      <p>This isn't the page,</p> <p> you are looking for...</p>
    </div>
  );
};

export default NotFound;
